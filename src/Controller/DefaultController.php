<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function default()
    {
        return $this->render('default/index.html.twig');
    }
    /**
     * @Route("/data", name="json")
     */
    public function index()
    {
        return new JsonResponse([
            [
                'id' => 1,
                'author' => 'Jack Wolters',
                'avatarUrl' => 'https://randomuser.me/api/portraits/thumb/men/12.jpg',
                'title' => 'Bitter Predation',
                'description' => 'Thirteen thin, round towers …',
            ],
            [
                'id' => 2,
                'author' => 'Hannah Johnson',
                'avatarUrl' => 'https://randomuser.me/api/portraits/thumb/women/12.jpg',
                'title' => 'Strangers of the Ambitious',
                'description' => "A huge gate with thick metal doors …",
            ],
            [
                'id' => 3,
                'author' => 'Tim Pickhart',
                'avatarUrl' => 'https://randomuser.me/api/portraits/thumb/men/35.jpg',
                'title' => 'Outsiders of the Mysterious',
                'description' => "Plain fields of a type of grass cover …",
            ],
        ]);
    }
}
