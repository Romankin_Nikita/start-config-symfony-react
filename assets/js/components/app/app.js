import React, {useState, useEffect} from 'react';
import './app.scss';

const App = () => {
  const [state, setState] = useState([]);

  useEffect(() => {
    fetch('/data')
      .then(response => response.json())
      .then(entries => {
        setState(
          entries
        );
      });
  }, []);

  return (
    <>
      {state.map(
        ({id, author, avatarUrl, title, description}) => (
          <div key={id} className='my-div'>
            <h2>{title}</h2>
            <h3>{author}</h3>
            <p>{description}</p>
            <img src={avatarUrl} alt="avatar" width={100} height={100}/>
            <br/>
          </div>
        )
      )}
    </>
  );
};

export default App;
